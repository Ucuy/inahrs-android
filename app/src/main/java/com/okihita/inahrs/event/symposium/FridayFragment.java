package com.okihita.inahrs.event.symposium;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.okihita.inahrs.R;
import com.okihita.inahrs.util.Config;
import com.okihita.inahrs.util.VolleySingleton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FridayFragment extends Fragment {

    @BindView(R.id.sympo_RV_dayTabbed)
    RecyclerView mFridayRV;
    List<Symposium> mFridaySymposia = new ArrayList<>();
    SymposiumAdapter mFridaySymposiaAdapter = new SymposiumAdapter(mFridaySymposia);

    public static FridayFragment newInstance() {
        Bundle args = new Bundle();
        FridayFragment fragment = new FridayFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.sympo_tabbed_fragment, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        setupRecyclerView();
        fetchFridaySymposia();
    }

    private void setupRecyclerView() {
        mFridayRV.setAdapter(mFridaySymposiaAdapter);
        mFridayRV.setLayoutManager(new LinearLayoutManager(this.getActivity()));
    }

    private void fetchFridaySymposia() {
        mFridaySymposia = new ArrayList<>();
        JsonArrayRequest sympoListRequest = new JsonArrayRequest(
                Config.SYMPOSIUM_GET_LIST,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        try {
                            for (int i = 0; i < response.length(); i++) {
                                JSONObject object = response.getJSONObject(i);
                                if (object.getString("date").equals("2016-10-07"))
                                    mFridaySymposia.add(new Symposium(object));
                            }
                            mFridaySymposiaAdapter = new SymposiumAdapter(mFridaySymposia);
                            setupRecyclerView();
                        } catch (JSONException e) {
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                    }
                }
        );
        VolleySingleton.getInstance(this.getActivity()).addToRequestQueue(sympoListRequest);
    }
}
