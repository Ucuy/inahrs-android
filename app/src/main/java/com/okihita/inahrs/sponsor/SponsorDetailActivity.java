package com.okihita.inahrs.sponsor;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.bumptech.glide.Glide;
import com.okihita.inahrs.R;
import com.okihita.inahrs.util.Config;
import com.okihita.inahrs.util.VolleySingleton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SponsorDetailActivity extends AppCompatActivity {

    public static final String SPONSOR_ID = "com.okihita.inahrs.sponsor_id";

    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.banner)
    ImageView mBannerImageView;
    @BindView(R.id.sponsorDetail_IV_image)
    ImageView mSponsorImageView;
    @BindView(R.id.sponsorDetail_TV_name)
    TextView mNameTextView;
    @BindView(R.id.sponsorDetail_TV_description)
    TextView mDescriptionTextView;
    @BindView(R.id.sponsorDetail_TV_address)
    TextView mAddressTextView;
    @BindView(R.id.sponsorDetail_TV_phone)
    TextView mPhoneTextView;
    @BindView(R.id.sponsorDetail_TV_email)
    TextView mEmailTextView;
    @BindView(R.id.sponsorDetail_TV_website)
    TextView mWebsiteTextView;
    ProgressDialog mProgressDialog;
    private int mSponsorId;

    @OnClick(R.id.toolbar_back)
    void finishing() {
        finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sponsor_detail);
        ButterKnife.bind(this);
        mSponsorId = getIntent().getIntExtra(SPONSOR_ID, 0);

        setupProgressDialog();
        setupToolbar();
        loadBannerImage();
        fetchSponsorDetail();
    }

    private void setupProgressDialog() {
        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setMessage("Loading...");
    }

    private void fetchSponsorDetail() {
        mProgressDialog.show();
        JsonArrayRequest sponsorDetailRequest = new JsonArrayRequest(
                Config.SPONSOR_ENDPOINT + String.valueOf(mSponsorId),
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        try {
                            updateSponsorView(response);
                        } catch (JSONException e) {
                            Log.e("###", "onResponse: " + e.getMessage());
                        }

                        mProgressDialog.dismiss();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(SponsorDetailActivity.this, "Sorry an error happened.", Toast.LENGTH_SHORT).show();
                    }
                }
        );

        VolleySingleton.getInstance(this).addToRequestQueue(sponsorDetailRequest);
    }

    private void updateSponsorView(JSONArray response) throws JSONException {
        JSONObject object = response.getJSONObject(0);

        Glide.with(this).load(Config.ENDPOINT + object.getString("sponsor_image"))
                .into(mSponsorImageView);
        mNameTextView.setText(object.getString("sponsor_name"));
        mDescriptionTextView.setText(object.getString("sponsor_description"));
        mAddressTextView.setText(object.getString("sponsor_address"));
        mPhoneTextView.setText(object.getString("sponsor_phone"));
        mWebsiteTextView.setText(object.getString("sponsor_website"));
        mEmailTextView.setText(object.getString("sponsor_email"));
    }

    private void setupToolbar() {
        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle(null);
    }

    private void loadBannerImage() {
        Glide.with(this).
                load(R.drawable.header_banner_without_pic_placeholder).
                into(mBannerImageView);
    }


    @OnClick(R.id.sponsorDetail_TV_phone)
    void callNumber() {

        String mPhoneNumber = mPhoneTextView.getText().toString();
        String uri = "tel:" + mPhoneNumber.trim();
        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse(uri));
        startActivity(intent);
    }

    @OnClick(R.id.sponsorDetail_TV_email)
    void sendEmail() {

        String emailAddress = mEmailTextView.getText().toString();
        Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                "mailto", emailAddress, null));
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "From InaHRS 2016");
        startActivity(Intent.createChooser(emailIntent, "Send email..."));
    }

    @OnClick(R.id.sponsorDetail_TV_website)
    void gotoWebsite() {
        String websiteAddress = mWebsiteTextView.getText().toString();
        Uri uriUrl = Uri.parse(websiteAddress);
        Intent launchBrowser = new Intent(Intent.ACTION_VIEW, uriUrl);
        startActivity(launchBrowser);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @OnClick(R.id.sponsorFooter_aphrs)
    void gotoAphrs() {
        Uri uriUrl = Uri.parse("http://www.aphrs.org/");
        Intent launchBrowser = new Intent(Intent.ACTION_VIEW, uriUrl);
        startActivity(launchBrowser);
    }

    @OnClick(R.id.sponsorFooter_perki)
    void gotoPerki() {
        Uri uriUrl = Uri.parse("http://www.inaheart.org/");
        Intent launchBrowser = new Intent(Intent.ACTION_VIEW, uriUrl);
        startActivity(launchBrowser);
    }

}